from django.urls import path
from . views import send_email
from . views import register
from . views import reset_password
from . views import send_email_to_all
from . views import send_newsletter
from . views import password_changed_notification
from . views import email_changed_notification

urlpatterns = [
    path('', send_email),
    path('register/', register),
    path('resetpassord', reset_password),
    path('sendtoall', send_email_to_all),
    path('newsletter', send_newsletter),
    path('passwordchanged', password_changed_notification),
    path('emailchanged', email_changed_notification)

]
