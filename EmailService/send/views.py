from django.core.mail import send_mail
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
import requests
from rest_framework.utils import json


#method for sending an email. This method is uses by different rest methods.
def send(subject, message, sender, recipient):

    send_mail(subject,
              message,
              sender,
              [recipient])

#Method for sending a simple email to anyone from our domain
@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def send_email(request):

    subject = request.POST.get("subject", "")
    message = request.POST.get("message", "")
    sender = request.POST.get("sender", "")
    recipient = request.POST.get("recipient", "")

    send(subject, message, sender, recipient)
    return Response({"The email has been sent to " + recipient + "from " + sender + "with the subject: " + subject + ". And with the message: " + message}, status=HTTP_200_OK)

#Method for sending email to new users with activation link
@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def register(request):

    aktiveringskode = request.GET.get("aktiveringskode", "")

    subject = "Fullfør registreringen din hos Kollektivet"
    message = "Velkommen til Kollektivet \n For å fullføre registreringen trykker du på linken under \n \n https://kollektivet.app/aktiver?aktiveringskode=" + aktiveringskode + " \n Mvh Kollektivet \n support@kollektivet.app"
    sender = "ikke-svar@kollektivet.app"
    recipient = request.GET.get("recipient", "")

    send(subject, message, sender, recipient)
    return Response({"The email has been sent to " + recipient + "from " + sender + "with the subject: " + subject + ". And with the message: " + message}, status=HTTP_200_OK)

#Method for sending email with code for resetting a password
@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def reset_password(request):

    resetpassordkode = request.POST.get("resetpassordkode", "")

    subject = "Resette passordet for brukeren din i Kollektivet"
    message = "Vi har fått melding om at du ønsker å resette passordet ditt. Hvis dette ikke er tilfellet så er det bare å ignorere denne meldingen. \n" \
              "For å resette passordet så er det bare å trykke på linken under: \n \n" \
              "https://kollektivet.app/resetpassord?resetpassordkode=" + resetpassordkode + "\n \n Mvh Kollektivet \n support@kollektivet.app"
    sender = "ikke-svar@kollektivet.app"
    recipient = request.POST.get("recipient", "")

    send(subject, message, sender, recipient)
    return Response({
                    "The email has been sent to " + recipient + "from " + sender + "with the subject: " + subject + ". And with the message: " + message},
                    status=HTTP_200_OK)

#Method for sending email to everyone registered
@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def send_email_to_all(request):

    subject = request.POST.get("subject", "")
    message = request.POST.get("message", "")
    sender = request.POST.get("sender", "")

    #Sending the email to everyone in the email list
    url = "http://127.0.0.1:8081/api/EmailList/"
    response = requests.get(url).text
    emails = json.loads(response)
    for recipient in emails:
        send(subject, message, sender, recipient)

    return Response({"The email has been sent to everyone from " + sender + " with the subject: " + subject + ". And with the message: " + message})

#Method for sending newsletter.
@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def send_newsletter(request):

    subject = request.POST.get("subject", "")
    message = request.POST.get("message", "")
    sender = request.POST.get("sender", "")

    url = "http//127.0.0.1/api/newsletterlist/"
    response = requests.get(url).text
    emails = json.loads(response)
    for recipient in emails:
        send(subject, message, sender, recipient)

    return Response({"The email has been sent to everyone from " + sender + " with the subject: " + subject + ". And with the message: " + message})


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def password_changed_notification(request):

    aktiveringskode = request.POST.get("aktiveringskode", "")

    subject = "Passordet ditt er endret i kollektivet"
    message = "Hei, passordet ditt har blitt endret i kollektivet. Om du ikke har endret passordet så anbefaler vi at du tar kontakt med support på support@kollektivet.app" + "\n \n Mvh Kollektivet \n support@kollektivet.app"
    sender = "ikke-svar@kollektivet.app"
    recipient = request.POST.get("recipient", "")

    send(subject, message, sender, recipient)
    return Response({"The email has been sent to " + recipient + "from " + sender + "with the subject: " + subject + ". And with the message: " + message}, status=HTTP_200_OK)@csrf_exempt


@api_view(["POST"])
@permission_classes((AllowAny,))
def email_changed_notification(request):

    aktiveringskode = request.POST.get("aktiveringskode", "")

    subject = "Eposten din er endret i kollektivet"
    message = "Hei, eposten din har blitt endret i kollektivet. Om du ikke har endret eposten så anbefaler vi at du tar kontakt med support på support@kollektivet.app" + "\n \n Mvh Kollektivet \n support@kollektivet.app"
    sender = "ikke-svar@kollektivet.app"
    recipient = request.POST.get("recipient", "")

    send(subject, message, sender, recipient)
    return Response({"The email has been sent to " + recipient + "from " + sender + "with the subject: " + subject + ". And with the message: " + message}, status=HTTP_200_OK)
